# CyberCook teste para Full Stack

 1. Desenvolver uma API utilizando PHP que retorne algo similar ao arquivo **/cybercook.json**
 2. Desenvolver a página seguindo o PSD **home.psd**
    * Apresentar os dados requisitados na API no layout

## Instruções

As imagens que não estiverem no cybercook.json podem ser retiradas diretamente do PSD ou adquiridas no site https://cybercook.com.br.

Clone este projeto

```sh
git clone git@bitbucket.org:emidia/teste-front-end.git
```

Instale as dependências

```sh
npm install
```

E rode a aplicação

```sh
npm start
```

Após concluir subir o código no [Github](https://github.com) ou em outro VCS a sua escolha e enviar o link para rodrigo.zani@e-midia.com

## Requisitos obrigatórios

 - Backend em PHP
 - Responsivo 
 - Sêmantica web
 - Utilizar algum pré-processador CSS (Sass, Less, Stylus...)
 - Ser fiel ao PSD

### Requisitos Extras

 - Performance
 - SEO
 - Código limpo
 - Animações
 - RESTful
